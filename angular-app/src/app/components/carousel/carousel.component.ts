import { Component, Input, OnInit } from '@angular/core';
import { MapTo } from '@adobe/cq-angular-editable-components';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  @Input()
  images;

  constructor() { }

  ngOnInit() {
  }


}

const CarouselEditConfig = {
  emptyLabel: 'Carousel',

  isEmpty: function (componentData) {
    return !componentData || !componentData.displayMessage || componentData.displayMessage.trim().length < 1;
  }
};

MapTo('wknd-events/components/content/carousel')(CarouselComponent, CarouselEditConfig);
