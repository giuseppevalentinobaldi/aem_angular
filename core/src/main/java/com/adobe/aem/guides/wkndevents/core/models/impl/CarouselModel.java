package com.adobe.aem.guides.wkndevents.core.models.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import com.adobe.cq.export.json.ComponentExporter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

import com.adobe.cq.export.json.ExporterConstants;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.apache.sling.api.resource.Resource;

@Model(
  adaptables = {SlingHttpServletRequest.class, Resource.class }, 
  adapters = { ComponentExporter.class, CarouselModel.class }, 
  resourceType = CarouselModel.RESOURCE_TYPE,
  defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
@Exporter(name = ExporterConstants.SLING_MODEL_EXPORTER_NAME, extensions = ExporterConstants.SLING_MODEL_EXTENSION)
@JsonSerialize(as = CarouselModel.class)
public class CarouselModel implements ComponentExporter {


  final static Logger logger = Logger.getLogger(" " + CarouselModel.class);

  static final String RESOURCE_TYPE = "wknd-events/components/content/carousel";
  private static final String PREPEND_MSG = "carousel";

  @Inject
  public Resource imageList;

  @ChildResource
  public List<Image> images;

  @PostConstruct
  protected void init() {
    logger.info("PRESSSSS" + this.images);
  }


	public List<Image> getImages() {
		return this.images;
	}

  public void setImages(List<Image> images) {
		this.images = images;
  }

  public String getDisplayMessage() {
    if(this.images != null && this.images.size() > 0) {
      return PREPEND_MSG + " "  + this.images;
    }
    return null;
  }

  @Override
  public String getExportedType() {
    return RESOURCE_TYPE;
  }

}