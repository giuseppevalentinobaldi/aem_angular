package com.adobe.aem.guides.wkndevents.core.models.impl;

// import com.fasterxml.jackson.databind.annotation.JsonSerialize;
// import javax.inject.Inject;
// import javax.inject.Named;
// import com.adobe.cq.export.json.ExporterConstants;
// import org.apache.sling.api.SlingHttpServletRequest;
// import org.apache.sling.models.annotations.Exporter;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.api.resource.Resource;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class Image {

  @ValueMapValue()
	private String src;

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}
		
}

